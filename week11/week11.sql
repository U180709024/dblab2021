call selectAllCustomers();


call getCustomersByCountry("Spain");

set @country = "Uk";
select @country;
call getCustomersByCountry(@country);
select @country;

select * From shippers;

Select orderID From orders Join shippers On orders.ShipperID = shippers.ShipperID Where ShipperName = "Speedy Express";
Select Count(OrderID) From orders Join shippers on orders.ShipperID = shippers.ShipperID Where ShipperName = "Speedy Express";

set @orderCount = 0;
call getNumberOfOrdersByShipper("Speedy Express", @orderCount);
select @orderCount;

set @beg = 100;
set @inc = 10;
call counter(@beg, @inc);
select @beg, @inc;


#week11
Select * From movies;
Select * From denormalized;

Load Data Infile "C:\\Users\\ff\\Desktop\\Recitation Materials-20211206\\denormalized.csv" into Table denormalized Columns Terminated By ";";Show variables like "secure_file_priv";
Load Data Infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized.csv" 
into Table denormalized 
Columns Terminated By ";";

Select Distinct movie_id, title, ranking, rating, year, votes, duration, oscars, budget From denormalized;

Insert Into movies (movie_id, title, ranking, rating, year, votes, duration, oscars, budget) Select Distinct movie_id, title, ranking, 
	rating, year, votes, duration, oscars, budget From denormalized;
Select * From movies;

Select Distinct producer_country_id, producer_country_name From denormalized
Union
Select Distinct director_country_id, director_country_name From denormalized
Union
Select Distinct star_country_id, star_country_name From denormalized Order By producer_country_id;

Insert Into countries (country_id, country_name) Select Distinct producer_country_id, producer_country_name From denormalized
Union
Select Distinct director_country_id, director_country_name From denormalized
Union
Select Distinct star_country_id, star_country_name From denormalized Order By producer_country_id;
Select * From countries;